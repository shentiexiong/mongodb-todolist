//jshint esversion:6

const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const app = express();

app.set("view engine", "ejs");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public"));

mongoose.connect("mongodb://localhost:27017/todoDB", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const itemsSchema = {
  name: String,
};

const Item = mongoose.model("Item", itemsSchema);
const item11 = new Item({
  name: "Wellcom Simn",
});
const item12 = new Item({
  name: "Wellcom JJJ",
});
const item13 = new Item({
  name: "Wellcom SS",
});

const defaultItems = [item11, item12, item13];

const listSchema = {
  name: String,
  items:[itemsSchema]
};

const List = mongoose.model("List", listSchema);


app.get("/", function (req, res) {
  Item.find({}, function (err, foundItems) {
    // console.log(foundItems);
    if (foundItems.length === 0) {
      Item.insertMany(defaultItems, function (err) {
        if (err) {
          console.log(err);
        } else {
          console.log("success");
        }
      });
      res.redirect("/");
    } else {
      res.render("list", { listTitle: "Today", newListItems: foundItems });
    }
  });
});

app.get("/:customListName", function (req, res) {
const customListName = req.params.customListName;

List.findOne({name:customListName}, function(err, foundList){
  if(!err){
    if(!foundList){
      //creat new list
      const list = new List({
        name: customListName,
        items: defaultItems
      }) ;
      list.save();
      res.redirect("/"+ customListName)
    }else {
      // show an existing list
      res.render("list", { listTitle: foundList.name, newListItems: foundList.items });
    }
  }
})


});

app.post("/", function (req, res) {
  const itemName = req.body.newItem;
  const listName = req.body.list;
  const item = new Item({
    name: itemName,
  });
if(listName ==="Today"){
  item.save();
  res.redirect("/");
} else{
  List.findOne({name: listName}, function(err,foundList){
    foundList.items.push(item);
    foundList.save();
    res.redirect("/" + listName);
  })
}



});

app.post("/delete", function(req, res){
  // console.log(req.body.checkbox);
  const checkedItemId =req.body.checkbox;
  Item.findByIdAndRemove(checkedItemId,function(err){
    if(!err){
      console.log("successfully delet this item");
      res.redirect("/");

    }
  })
})



app.get("/about", function (req, res) {
  res.render("about");
});

app.listen(3000, function () {
  console.log("Server started on port 3000");
});
